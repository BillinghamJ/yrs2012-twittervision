<?php
function getFileUrl($url)
{
	$filename = 'cache/' . md5($url);
	
	if (!file_exists($filename))
	{
		$contents = file_get_contents($url);
		file_put_contents($filename, $contents);
	}

	return $filename;
}

// The URL to get
//$url = 'http://search.twitter.com/search.json?q=from:cheshirepolice+cctv+appeal+filter:links&include_entities=false&result_type=recent';

$url = 'cache/292c8f6632a5baf8bff6d4cad1277165';

// Do the search
$result = file_get_contents($url);

// convert JSON to actual object
$parsed = json_decode($result);

$output = array();

foreach ($parsed->results as $result) {
	$text = $result->text;

	$message = explode('http://', $text);

	$text = $message[0];
	$url = 'http://' . $message[1];

	$page = file_get_contents(getFileUrl($url));
	$index = strpos($page, '<p><img');
	$index = strpos($page, 'src="', $index) + 5;
	$indexend = strpos($page, '"', $index);
	$image = 'http://www.cheshire.police.uk' . substr($page, $index, $indexend - $index);

	array_push($output, array('text' => $text, 'image' => $image));
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Closed Circuit TwitterVision</title>
	<meta name="author" content="Ste Brennan - Code Computerlove - http://www.codecomputerlove.com/" />
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" name="viewport" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<link href="styles.css" type="text/css" rel="stylesheet" />
	
	<link href="photoswipe.css" type="text/css" rel="stylesheet" />
	
	<script type="text/javascript" src="lib/klass.min.js"></script>
	<script type="text/javascript" src="code.photoswipe-3.0.5.min.js"></script>
	
	
	<script type="text/javascript">

		(function(window, PhotoSwipe){
		
			document.addEventListener('DOMContentLoaded', function(){
			
				var
					options = {},
					instance = PhotoSwipe.attach( window.document.querySelectorAll('#Gallery a'), options );
			
			}, false);
			
			
		}(window, window.Code.PhotoSwipe));
		
	</script>
		
<script type="text/javascript">
  var GoSquared = {};
  GoSquared.acct = "GSN-352498-F";
  (function(w){
    function gs(){
      w._gstc_lt = +new Date;
      var d = document, g = d.createElement("script");
      g.type = "text/javascript";
      g.src = "//d1l6p2sc9645hc.cloudfront.net/tracker.js";
      var s = d.getElementsByTagName("script")[0];
      s.parentNode.insertBefore(g, s);
    }
    w.addEventListener ?
      w.addEventListener("load", gs, false) :
      w.attachEvent("onload", gs);
  })(window);
</script>
</head>
<body>



<div id="MainContent">

	<div class="page-content">
		<h1>Closed Circuit TwitterVision</h1>
	</div>
	
	<ul id="Gallery" class="gallery">
		<?php foreach ($output as $tweet) { ?>
			<li><a href="<?php echo $tweet['image']; ?>"><img src="<?php echo $tweet['image']; ?>" alt="<?php echo $tweet['text']; ?>" /></a></li>
		<?php } ?>
	</ul>
	
</div>	



</body>
</html>

